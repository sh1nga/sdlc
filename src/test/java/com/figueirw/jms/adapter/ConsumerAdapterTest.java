package com.figueirw.jms.adapter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.net.UnknownHostException;

import static org.junit.Assert.*;

public class ConsumerAdapterTest {

    String json = "{vendorName:\"Microsofttest\",firstName:\"BobTest\",lastName:\"SmithTest\",address:\"123 Main test\",city:\"TulsaTest\",state:\"OKTest\",zip:\"71345Test\",email:\"Bob@microsoft.test\",phoneNumber:\"test-123-test\"}";

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void sendToMongo() throws UnknownHostException {
        ConsumerAdapter consumerAdapter = new ConsumerAdapter();
        consumerAdapter.sendToMongo(json);

        assertNotNull(json);
    }
}