package com.figueirw.jms.listener;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.jms.TextMessage;

import static org.junit.Assert.*;

public class ConsumerListenerTest {

    private TextMessage message;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void onMessage() {
        ConsumerListener listener = new ConsumerListener();
        listener.onMessage(message);
        assertNull(message);
    }
}