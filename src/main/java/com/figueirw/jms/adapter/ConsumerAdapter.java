package com.figueirw.jms.adapter;

import com.figueirw.jms.listener.ConsumerListener;
import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;
import com.mongodb.util.JSONParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.stereotype.Component;

import java.net.UnknownHostException;
import java.util.logging.Level;

@EnableAutoConfiguration(exclude = {MongoAutoConfiguration.class})
@Component
public class ConsumerAdapter {
    private static Logger logger = LogManager.getLogger(ConsumerAdapter.class.getName());


    public void sendToMongo(String json) throws UnknownHostException  {
        logger.info("Sending to MongoDB");

        MongoClient mongoClient = new MongoClient();
        DB mongoDB = mongoClient.getDB("vendor");
        DBCollection contact = mongoDB.getCollection("contact");
        logger.info("Converting JSON to DBObject");

        DBObject dbObject = (DBObject)JSON.parse(json);
        contact.insert(dbObject);
        logger.info("Sent to MongoDB");
    }
}
